#!/usr/bin/env python3

"""
Demonstration for minimal b-tagging

This tags jets in two ways:

- By creating a new BTagging object, and then associating tracks and
  running a tagger.

- Directly on the jet. No BTagging object is created, all the
  variables are added to the jet directly.

Unlike most code running in this framework, we avoid running DL2 in
the dataset dumper itself. This is just to provide an example: if
you're trying to use this code to study new NN trainings we recommend
configuring DL2 as part of the dataset dumper json configuration.
"""

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg
)

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from GaudiKernel.Configurable import DEBUG, INFO

from BTagTrainingPreprocessing import dumper

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter as Formatter
import sys

def get_args():
    parser = ArgumentParser(description=__doc__, formatter_class=Formatter)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='output.h5')
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-d','--debug', action='store_true')
    parser.add_argument('-i','--event-print-interval', type=int, default=100)
    return parser.parse_args()

def getSimpleBTagConfig(flags, nnFile):
    """
    This example builds a BTagging object, and associates tracks. The
    tagging is left for the dumper code here.
    """
    ca = ComponentAccumulator()

    btag_name = 'TestBTagging'
    jet_name = 'AntiKt4EMPFlowJets'
    track_name = "InDetTrackParticles"
    builder = CompFactory.FlavorTagDiscriminants.BTaggingBuilderAlg(
        'btagbuilder',
        jetName=jet_name,
        btaggingName=btag_name)
    ca.addEventAlgo(builder)

    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        'PoorMansIpAugmenterAlg',
        trackContainer=track_name,
    ))

    ca.addEventAlgo(CompFactory.JetToBTagLinkerAlg(
        'jetToBtag',
        oldLink=f'{btag_name}.jetLink',
        newLink=f'{jet_name}.newBtaggingLink'
    ))

    track_decor = "TracksForMinimalTag"
    ca.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=jet_name,
        InputParticleCollection=track_name,
        OutputParticleDecoration=track_decor
    ))

    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.BTagTrackLinkCopyAlg(
            'copy_alg',
            jetTracks=f'{jet_name}.{track_decor}',
            btagTracks=f'{btag_name}.BTagTrackToJetAssociator',
            jetLinkName=f'{btag_name}.jetLink'
        )
    )

    # Now we have to add an algorithm that tags the jets with dips
    # The input and output remapping is handled via a map in DL2.
    #
    # The file above adds dipsLoose20210517_p*, we'll call them
    # dips_p* on the btag object.
    variableRemapping = {
        **{f'dipsLoose20210517_p{x}': f'dips_p{x}' for x in 'cub'},
        'btagIp_': 'poboyIp_',
    }
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.BTagDecoratorAlg(
            'simpleBTagAlg',
            container=btag_name,
            constituentContainer=track_name,
            decorator=CompFactory.FlavorTagDiscriminants.DL2Tool(
                'simpleDips',
                nnFile=nnFile,
                variableRemapping=variableRemapping,
                # note that the tracks are associated to BTagging as
                # a TrackParticle container.
                trackLinkType='TRACK_PARTICLE',
            ),
        )
    )
    return ca

def getSimpleJetTagConfig(flags, nnFile):
    """
    This example tags jets directly: there is no intermediate
    b-tagging object
    """

    ca = ComponentAccumulator()

    # first add the track augmentation to define peragee coordinates
    jet_name = 'AntiKt4EMPFlowJets'
    trackContainer = 'InDetTrackParticles'
    primaryVertexContainer = 'PrimaryVertices'
    simpleTrackIpPrefix = 'simpleIp_'
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            'SimpleTrackAugmenter',
            trackContainer=trackContainer,
            primaryVertexContainer=primaryVertexContainer,
            prefix=simpleTrackIpPrefix,
        )
    )

    # now we assicoate the tracks to the jet
    tracksOnJetDecoratorName = "TracksForMinimalJetTag"
    ca.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=jet_name,
        InputParticleCollection=trackContainer,
        OutputParticleDecoration=tracksOnJetDecoratorName,
    ))

    # Now we have to add an algorithm that tags the jets with dips
    # The input and output remapping is handled via a map in DL2.
    #
    # The file above adds dipsLoose20210517_p*, we'll call them
    # dips_p* on the jet.
    variableRemapping = {
        'BTagTrackToJetAssociator': tracksOnJetDecoratorName,
        **{f'dipsLoose20210517_p{x}': f'dipsOnJet_p{x}' for x in 'cub'},
        'btagIp_': simpleTrackIpPrefix,
    }
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.JetTagDecoratorAlg(
            'simpleJetTagAlg',
            container=jet_name,
            constituentContainer=trackContainer,
            decorator=CompFactory.FlavorTagDiscriminants.DL2Tool(
                'simpleDipsToJet',
                nnFile=nnFile,
                variableRemapping=variableRemapping,
                # note that the tracks are associated to the jet as
                # and IParticle container.
                trackLinkType='IPARTICLE',
            ),
        )
    )
    return ca

def getSingleBTagConfig(flags, config, output):

    ca = ComponentAccumulator()

    nnFile = 'dev/BTagging/20210517/dipsLoose/antikt4empflow/network.json'

    ca.merge(getSimpleBTagConfig(flags, nnFile))
    ca.merge(getSimpleJetTagConfig(flags, nnFile))
    ca.merge(dumper.getDumperConfig(config, output))

    return ca


def run():
    args = get_args()

    cfgFlags.Input.Files = args.input_files
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    cfgFlags.Exec.OutputLevel = INFO
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG
        cfgFlags.Exec.DebugStage = 'exec'

    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(getSingleBTagConfig(cfgFlags, args.config_file, args.output))

    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
