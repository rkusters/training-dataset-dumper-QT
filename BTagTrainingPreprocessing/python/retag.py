#######################################################################
# Here we construct a retagging sequence that takes a few variables
# Such as :
#   the input track, jet collections
#   list of tracking systematics to run over
#   option to merge LRT and standard tracks
# This allows the users to perform retagging with more flexibilities  
######################################################################

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentAccumulator import conf2toConfigurable
from BTagTrainingPreprocessing.trackUtil import LRTMerger, applyTrackSys


def RenameInputContainerCfgExample(suffix, JetCollectionShort, tracksKey = 'InDetTrackParticles'):
    acc=ComponentAccumulator()
    # Delete BTagging container read from input ESD
    AddressRemappingSvc, ProxyProviderSvc=CompFactory.getComps("AddressRemappingSvc","ProxyProviderSvc",)
    AddressRemappingSvc = AddressRemappingSvc("AddressRemappingSvc")
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::JetAuxContainer#' + JetCollectionShort + 'Jets.BTagTrackToJetAssociator->' + JetCollectionShort + 'Jets.BTagTrackToJetAssociator_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::JetAuxContainer#' + JetCollectionShort + 'Jets.JFVtx->' + JetCollectionShort + 'Jets.JFVtx_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::JetAuxContainer#' + JetCollectionShort + 'Jets.SecVtx->' + JetCollectionShort + 'Jets.SecVtx_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::JetAuxContainer#' + JetCollectionShort + 'Jets.btaggingLink->' + JetCollectionShort + 'Jets.btaggingLink_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::BTaggingContainer#BTagging_' + JetCollectionShort + '->BTagging_' + JetCollectionShort + '_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::BTaggingAuxContainer#BTagging_' + JetCollectionShort + 'Aux.->BTagging_' + JetCollectionShort + '_' + suffix+"Aux."]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::VertexContainer#BTagging_' + JetCollectionShort + 'SecVtx->BTagging_' + JetCollectionShort + 'SecVtx_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::VertexAuxContainer#BTagging_' + JetCollectionShort + 'SecVtxAux.->BTagging_' + JetCollectionShort + 'SecVtx_' + suffix+"Aux."]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::BTagVertexContainer#BTagging_' + JetCollectionShort + 'JFVtx->BTagging_' + JetCollectionShort + 'JFVtx_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::BTagVertexAuxContainer#BTagging_' + JetCollectionShort + 'JFVtxAux.->BTagging_' + JetCollectionShort + 'JFVtx_' + suffix+"Aux."]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.TrackCompatibility->' + tracksKey + '.TrackCompatibility_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.btagIp_d0->' + tracksKey + '.btagIp_d0_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.btagIp_z0SinTheta->' + tracksKey + '.btagIp_z0SinTheta_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.btagIp_d0Uncertainty->' + tracksKey + '.btagIp_d0Uncertainty_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.btagIp_z0SinThetaUncertainty->' + tracksKey + '.btagIp_z0SinThetaUncertainty_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.btagIp_trackMomentum->' + tracksKey + '.btagIp_trackMomentum_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.btagIp_trackDisplacement->' + tracksKey + '.btagIp_trackDisplacement_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::TrackParticleAuxContainer#' + tracksKey + '.JetFitter_TrackCompatibility_antikt4empflow->' + tracksKey + '.JetFitter_TrackCompatibility_antikt4empflow_' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::JetAuxContainer#' + JetCollectionShort + 'Jets.TracksForBTagging->' + JetCollectionShort + 'Jets.TracksForBTagging' + suffix]
    AddressRemappingSvc.TypeKeyRenameMaps += ['xAOD::JetAuxContainer#' + JetCollectionShort + 'Jets.MuonsForBTagging->' + JetCollectionShort + 'Jets.MuonsForBTagging' + suffix]
    acc.addService(AddressRemappingSvc)
    acc.addService(ProxyProviderSvc(ProviderNames = [ "AddressRemappingSvc" ]))
    return acc


def setupCondDb(cfgFlags, taggerList):
    from AthenaCommon.AppMgr import athCondSeq
    if not hasattr(athCondSeq,"JetTagCalibCondAlg"):
        CalibrationChannelAliases = ["AntiKt4EMPFlow->AntiKt4EMPFlow,AntiKt4EMTopo,AntiKt4TopoEM,AntiKt4LCTopo"]
        grades= cfgFlags.BTagging.Grades
        RNNIPConfig = {'rnnip':''}

        JetTagCalibCondAlg=CompFactory.Analysis.JetTagCalibCondAlg
        jettagcalibcondalg = "JetTagCalibCondAlg"
        readkeycalibpath = "/GLOBAL/BTagCalib/RUN12"
        connSchema = "GLOBAL_OFL"
        if not cfgFlags.Input.isMC:
            readkeycalibpath = readkeycalibpath.replace("/GLOBAL/BTagCalib","/GLOBAL/Onl/BTagCalib")
            connSchema = "GLOBAL"
        histoskey = "JetTagCalibHistosKey"
        from IOVDbSvc.CondDB import conddb

        conddb.addFolder(connSchema, readkeycalibpath, className='CondAttrListCollection')
        JetTagCalib = JetTagCalibCondAlg(jettagcalibcondalg, ReadKeyCalibPath=readkeycalibpath, HistosKey = histoskey, taggers = taggerList,
            channelAliases = CalibrationChannelAliases, IP2D_TrackGradePartitions = grades, RNNIP_NetworkConfig = RNNIPConfig)

        athCondSeq+=conf2toConfigurable( JetTagCalib, indent="  " )

def retagging(cfgFlags, merge, input_track = "InDetTrackParticles", input_jet = "AntiKt4EMPFlowJets", sys_list = []):
  
    acc = ComponentAccumulator()

    from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg

    taggerList = ['IP2D', 'IP3D', 'SV1']
    setupCondDb(cfgFlags, taggerList)


    from BTagging.JetParticleAssociationAlgConfig import JetParticleAssociationAlgCfg
    from BTagging.JetBTaggingAlgConfig import JetBTaggingAlgCfg
    from BTagging.JetSecVertexingAlgConfig import JetSecVertexingAlgCfg
    from BTagging.JetSecVtxFindingAlgConfig import JetSecVtxFindingAlgCfg
    from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
    from BTagging.BTagHighLevelAugmenterAlgConfig import BTagHighLevelAugmenterAlgCfg
    from BTagging.HighLevelBTagAlgConfig import HighLevelBTagAlgCfg

    pvCol = 'PrimaryVertices'
    jetCol = input_jet
    muon_collection = 'Muons'
    track_collection = input_track
    SecVertexers = [ 'JetFitter' , 'SV1' ]
    
    postTagDL2JetToTrainingMap={
        'AntiKt4EMPFlow': [
           'BTagging/201903/rnnip/antikt4empflow/network.json',
           'BTagging/201903/dl1r/antikt4empflow/network.json',
           'BTagging/20210729/dipsLoose/antikt4empflow/network.json', #new r22 trainings
           'BTagging/20210729/dips/antikt4empflow/network.json',
           'BTagging/20210824r22/dl1dLoose/antikt4empflow/network.json', #recommended tagger which is DL1dLoose20210824r22 named DL1dv00 in EDM
           'BTagging/20210824r22/dl1d/antikt4empflow/network.json',
           'BTagging/20210824r22/dl1r/antikt4empflow/network.json',
        ],
    }
       
    # Run the track systematic tools
    if sys_list and merge:
      print ("Track merging and track systematic variations can not be used together at the moment. Aborting!")
      return -1
 
    if sys_list:
        acc.merge(applyTrackSys(sys_list, input_track, input_jet))
        track_collection = "InDetTrackParticles_Sys"

    # The decoration on tracks needs to happen before the track merging step
    # Decorate the standard tracks first 
    
    acc.merge(BTagTrackAugmenterAlgCfg(cfgFlags,  TrackCollection = track_collection, PrimaryVertexCollectionName = pvCol))
    
    if merge:
        # Decorate the LRT tracks and merge the LRT and standard tracks into a single view collection
        acc.merge(BTagTrackAugmenterAlgCfg(cfgFlags,  TrackCollection = "InDetLargeD0TrackParticles", PrimaryVertexCollectionName = pvCol))
        acc.merge(LRTMerger())
        track_collection = "InDetWithLRTTrackParticles" 

    jetCol_no_Jets = jetCol.replace('Jets','')
    BTaggingCollection = cfgFlags.BTagging.OutputFiles.Prefix + jetCol_no_Jets

    acc.merge(RenameInputContainerCfgExample('_retag',jetCol_no_Jets))
    
    acc.merge(JetTagCalibCfg(cfgFlags, TaggerList=taggerList))

    #Track Association
    acc.merge(JetParticleAssociationAlgCfg(cfgFlags, jetCol, track_collection, "TracksForBTagging"))
    acc.merge(JetParticleAssociationAlgCfg(cfgFlags, jetCol, muon_collection, "MuonsForBTagging"))

    for sv in SecVertexers:
        acc.merge(JetSecVtxFindingAlgCfg(cfgFlags, jetCol_no_Jets, "PrimaryVertices", sv, "TracksForBTagging"))
        acc.merge(JetSecVertexingAlgCfg(cfgFlags, BTaggingCollection, jetCol_no_Jets, track_collection,  "PrimaryVertices", sv))

    acc.merge(JetBTaggingAlgCfg( 
        cfgFlags
      , BTaggingCollection = BTaggingCollection
      , JetCollection = jetCol_no_Jets
      , PrimaryVertexCollectionName="PrimaryVertices"
      , TaggerList = taggerList
      , SecVertexers = SecVertexers
      , Tracks = "TracksForBTagging"
      , Muons = "MuonsForBTagging"
      )
    )

    AthSequencer=CompFactory.AthSequencer

    sequenceName = BTaggingCollection+ "_HLTaggers"

    HLBTagSeq = AthSequencer(sequenceName, Sequential = True)
    acc.addSequence(HLBTagSeq)

    acc.merge(BTagHighLevelAugmenterAlgCfg(cfgFlags, jetCol_no_Jets, BTagCollection=BTaggingCollection, Associator='BTagTrackToJetAssociator', TrackCollection=track_collection), sequenceName=sequenceName )
    for dl2 in postTagDL2JetToTrainingMap[jetCol_no_Jets]:
        acc.merge(HighLevelBTagAlgCfg(cfgFlags, BTaggingCollection, TrackCollection=track_collection, NNFile=dl2))

    return acc
