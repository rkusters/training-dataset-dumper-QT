from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
import os

def prepGdb():
    """
    Running gdb is sometimes more difficult than it should be, this
    runs some workarounds.
    """
    # Weird workaround for the debugger in images
    #
    # The environment variable I'm checking here just happens to be
    # set to a directory you would not have access to outside images.
    #
    if os.environ.get('LCG_RELEASE_BASE') == '/opt/lcg':
        del os.environ['PYTHONHOME']


def getDumperConfig(config, output_path):
    ca = ComponentAccumulator()

    output = CompFactory.H5FileSvc(path=output_path)
    ca.addService(output)

    btagAlg = CompFactory.SingleBTagAlg('DatasetDumper')
    btagAlg.output = output
    btagAlg.configFileName = config
    ca.addEventAlgo(btagAlg)

    return ca
