#include "TrackVertexDecorator.hh"


// the constructor just builds the decorator
TrackVertexDecorator::TrackVertexDecorator(const std::string& prefix):
  m_track_sv1_idx(prefix + "SV1VertexIndex"),
  m_track_jf_idx(prefix + "JFVertexIndex"),
  m_SV1VxAccessor("SV1_vertices"),
  m_jfVxAccessor("JetFitter_JFvertices"),
  m_AMVFVertices("TTVA_AMVFVertices"),
  m_AMVFWeights("TTVA_AMVFWeights"),
  m_AMVFWeightPV("AMVFWeightPV")
{
}

bool particleInCollection( const xAOD::TrackParticle& track, std::vector< ElementLink< xAOD::TrackParticleContainer>> track_collection ) {
  for ( auto test_track : track_collection ) {
    if (&track == *test_track) return true;
  }
  return false;
}

int get_sv1_index( const xAOD::TrackParticle& track, std::vector<ElementLink<xAOD::VertexContainer>>& sv1_vertices ) {
  int track_jf_idx = -2;
  for (unsigned int sv1_idx = 0; sv1_idx < sv1_vertices.size(); sv1_idx++) { 
    if (!sv1_vertices.at(sv1_idx).isValid()) {
      continue;
    }

    // jet tracks used in this vertex
    const xAOD::Vertex *tmpVertex = *(sv1_vertices.at(sv1_idx));
    const std::vector< ElementLink<xAOD::TrackParticleContainer>> tracks_in_vertices = tmpVertex->trackParticleLinks();

    // if the track was used, save the index
    if ( particleInCollection(track, tracks_in_vertices)) {
      track_jf_idx = sv1_idx;
      break;
    }
  }
  return track_jf_idx;
}

int get_jf_index( const xAOD::TrackParticle& track, std::vector<ElementLink<xAOD::BTagVertexContainer>>& jf_vertices ) {
  int track_jf_idx = -2;
  for (unsigned int jf_idx = 0; jf_idx < jf_vertices.size(); jf_idx++) { 
    if (!jf_vertices.at(jf_idx).isValid()) {
      continue;
    }

    // jet tracks used in this vertex
    const xAOD::BTagVertex *tmpVertex = *(jf_vertices.at(jf_idx));
    const std::vector< ElementLink<xAOD::TrackParticleContainer>> tracks_in_vertices = tmpVertex->track_links();

    // if the track was used, save the index
    if ( particleInCollection(track, tracks_in_vertices)) {
      track_jf_idx = jf_idx;
      break;
    }
  }
  return track_jf_idx;
}

float TrackVertexDecorator::get_AMVF_PV_weight( const xAOD::TrackParticle& track, 
                          const xAOD::Vertex& pv ) const {

  // accessors
  auto TTVA_AMVFVertices = m_AMVFVertices(track);
  auto TTVA_AMVFWeights  = m_AMVFWeights(track);

  // for each AMVF vertex
  for (unsigned int vtx_idx = 0; vtx_idx < TTVA_AMVFVertices.size(); vtx_idx++) { 

    if ( !TTVA_AMVFVertices.at(vtx_idx).isValid() ) {
      continue;
    }

    auto vertex = *(TTVA_AMVFVertices.at(vtx_idx));

    // is this the PV?
    if ( vertex == &pv ) {

      // return track weighting to the PV
      return TTVA_AMVFWeights.at(vtx_idx);
    }
  }

  // track must not have been used in the PV fit
  return 0.0;
}

// this call actually does the work on the track
void TrackVertexDecorator::decorate(const xAOD::TrackParticle& track, 
                                    const xAOD::BTagging& btag,
                                    const xAOD::Vertex& pv) const {

  // get vertex collections
  std::vector<ElementLink<xAOD::VertexContainer>> sv1_vertices = m_SV1VxAccessor(btag);
  std::vector<ElementLink<xAOD::BTagVertexContainer>> jf_vertices = m_jfVxAccessor(btag);

  // get index of vertex associated to this track
  int track_sv1_idx = get_sv1_index(track, sv1_vertices);
  int track_jf_idx = get_jf_index(track, jf_vertices);

  // store index
  m_track_sv1_idx(track) = track_sv1_idx;
  m_track_jf_idx(track)  = track_jf_idx;

  // add track PV matching weighting
  float AMVFWeightPV = get_AMVF_PV_weight(track, pv);
  m_AMVFWeightPV(track) = AMVFWeightPV;

}
