#ifndef TRUTH_CORRUPTION_COUNTER_HH
#define TRUTH_CORRUPTION_COUNTER_HH

#include <string>

namespace xAOD {
  class EventInfo_v1;
  using EventInfo = EventInfo_v1;
}

namespace truth {
  class TruthRecordError;
}

struct TruthCorruptionCounter {
  unsigned long long n_successful_truth_reads = 0;
  unsigned long long n_failed_truth_reads = 0;
};

void writeTruthCorruptionCounts(
  const TruthCorruptionCounter&,
  const std::string& output_file = "userJobMetadata.json");

std::string getEventInfoString(const truth::TruthRecordError& err,
                               const xAOD::EventInfo& info);

#endif
