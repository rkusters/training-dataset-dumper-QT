#ifndef TRUTH_SELECTOR_CONFIG_HH
#define TRUTH_SELECTOR_CONFIG_HH

#include <string>

struct TruthSelectorConfig
{
  float pt_minumum      = 2e3;
  float abs_eta_maximum = 2.5;
  float dr_maximum      = 0.4;
};

#endif
