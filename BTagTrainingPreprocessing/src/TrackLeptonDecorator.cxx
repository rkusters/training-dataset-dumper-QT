#include "TrackLeptonDecorator.hh"

#include "xAODEgamma/ElectronxAODHelpers.h"

// the constructor just builds the decorator
TrackLeptonDecorator::TrackLeptonDecorator(const std::string& prefix):
  m_track_lepton_id(prefix + "leptonID")
{
}


// this call actually does the work on the track
void TrackLeptonDecorator::decorate(const xAOD::TrackParticleContainer& tracks, const xAOD::MuonContainer& muons, const xAOD::ElectronContainer& electrons) const {

  // give all tracks a default starting value
  for ( const xAOD::TrackParticle* track : tracks ) {
    m_track_lepton_id(*track) = 0;
  }

  // loop over muons
  for ( const auto muon : muons ) {

    // get associated InDet track link
    auto track_link = muon->inDetTrackParticleLink();
    if ( !track_link.isValid() ) {
      continue;
    }

    // access track
    auto track = *track_link;

    // decorate the track
    m_track_lepton_id(*track) = -13 * muon->charge();
  }

  // loop over electrons
  for ( const auto electron : electrons ) {

    // get associated InDet track link (not the GSF track which is likely to have improved parameters,
    // more info: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EGammaD3PDtoxAOD#TrackParticle)
    const xAOD::TrackParticle* track = xAOD::EgammaHelpers::getOriginalTrackParticle(electron);

    if ( !track ) {
      continue;
    }

    // decorate the track
    m_track_lepton_id(*track) = -11 * electron->charge();
  }

}
