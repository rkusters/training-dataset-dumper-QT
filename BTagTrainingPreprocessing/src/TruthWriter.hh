#ifndef TRUTH_WRITER_HH
#define TRUTH_WRITER_HH

#include "TrackSortOrder.hh"

#include "xAODJet/JetFwd.h"
#include "xAODTruth/TruthParticleFwd.h"
#include "xAODTruth/TruthParticleContainerFwd.h"

#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"

// Standard Library things
#include <string>
#include <vector>
#include <memory>

namespace H5 {
  class Group;
}

class TruthOutputWriter;
class TruthConsumers;

struct TruthOutputs {
  const xAOD::TruthParticle* truth;
  const xAOD::Jet* jet;
};

class TruthWriter
{
public:
  typedef SG::AuxElement AE;
  typedef std::vector<const xAOD::TruthParticle*> Truths;
  typedef ElementLink<xAOD::TruthParticleContainer> TruthLink;
  typedef std::vector<TruthLink> TruthLinks;

  TruthWriter(
    H5::Group& output_file,
    const std::size_t output_size,
    const std::string& link_name,
    TrackSortOrder order);

  ~TruthWriter();
  TruthWriter(TruthWriter&) = delete;
  TruthWriter operator=(TruthWriter&) = delete;
  TruthWriter(TruthWriter&&);

  void write(const xAOD::Jet& jet);
  void write_dummy();

private:
  template<typename I, typename O = I>
  void add_truth_fillers(TruthConsumers&,
                         const std::vector<std::string>&,
                         O def_value);

  std::unique_ptr<TruthOutputWriter> m_hdf5_truth_writer;
  AE::ConstAccessor<TruthLinks> m_acc;
  const std::size_t& m_output_size;

  std::function<bool(const xAOD::TruthParticle* p1, const xAOD::TruthParticle* p2)> m_sort;

  Truths get_truth_parts(const xAOD::Jet& jet) const;
};

#endif
