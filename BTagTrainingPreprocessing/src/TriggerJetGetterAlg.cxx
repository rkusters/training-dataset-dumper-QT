#include "TriggerJetGetterAlg.h"
#include "AsgMessaging/MessageCheck.h"

#include "xAODJet/JetAuxContainer.h"
#include "xAODBTagging/BTaggingAuxContainer.h"

#include "xAODBTagging/BTaggingUtilities.h"

#include <memory>


TriggerJetGetterAlg::TriggerJetGetterAlg(const std::string& name,
                                         ISvcLocator* pSvcLocator):
  EL::AnaAlgorithm(name, pSvcLocator),
  m_jetLink("jetLink"),
  m_btagLink("btaggingLink")
{
}

StatusCode TriggerJetGetterAlg::initialize() {
  // get handle to TrigDecisionTool
  ATH_CHECK(m_triggerDecision.retrieve());
  ATH_CHECK(m_jetModifiers.retrieve());
  for (auto& handle: m_jetModifiers) {
    ATH_CHECK(handle.retrieve());
  }
  ATH_CHECK(m_trigJetKey.initialize());
  ATH_CHECK(m_outputJets.initialize());
  ATH_CHECK(m_outputBTag.initialize());

  return StatusCode::SUCCESS;
}
StatusCode TriggerJetGetterAlg::execute () {

  const EventContext& context = Gaudi::Hive::currentContext();
  SG::WriteHandle outputJets(m_outputJets, context);
  ATH_CHECK(outputJets.record(
              std::make_unique<xAOD::JetContainer>(),
              std::make_unique<xAOD::JetAuxContainer>()));

  SG::WriteHandle outputBTags(m_outputBTag, context);
  ATH_CHECK(outputBTags.record(
              std::make_unique<xAOD::BTaggingContainer>(),
              std::make_unique<xAOD::BTaggingAuxContainer>()));

  const std::string& chain = m_bjetChain;
  if( m_triggerDecision->isPassed(chain) ){
    ATH_MSG_DEBUG("Chain passed " << chain);
  } else{
    ATH_MSG_DEBUG("Chain did not pass " << chain);
    return StatusCode::SUCCESS;
  }

  // logic copied from here:
  // https://gitlab.cern.ch/cnass/athena/-/blob/ARTtrigger/PhysicsAnalysis/JetTagging/FlavourTaggingTests/src/PhysicsTriggerVariablePlots.cxx#L121
  ATH_MSG_DEBUG("Getting jets with key " << m_trigJetKey.key());
  auto jets = m_triggerDecision->features<xAOD::JetContainer>(
    chain, TrigDefs::Physics, m_trigJetKey.key());
  ATH_MSG_DEBUG("Got " << jets.size() << " jets");
  for (const auto& jet_link: jets) {
    const xAOD::Jet* trig_jet = *jet_link.link;
    // TODO: can we just push trig_jet into the output vector without
    // making a new one?
    auto jet = std::make_unique<xAOD::Jet>();
    auto* jetp = jet.get();
    outputJets->push_back(std::move(jet));
    *jetp = *trig_jet;

    // Now add the b-tagging object
    const xAOD::BTagging* trig_btag = xAOD::BTaggingUtilities::getBTagging(
        *trig_jet);
    auto btag = std::make_unique<xAOD::BTagging>();
    auto* btagp = btag.get();
    outputBTags->push_back(std::move(btag));
    *btagp = *trig_btag;

    // and link them together
    ElementLink<xAOD::JetContainer> jetLink(
      m_outputJets.hashedKey(),
      outputJets->back()->index(), context);
    m_jetLink(*outputBTags->back()) = jetLink;
    ElementLink<xAOD::BTaggingContainer> btagLink(
      m_outputBTag.hashedKey(),
      outputBTags->back()->index(), context);
    m_btagLink(*outputJets->back()) = btagLink;
  }
  for (const auto& tool: m_jetModifiers) {
    ATH_CHECK(tool->modify(*outputJets));
  }
  return StatusCode::SUCCESS;
}
StatusCode TriggerJetGetterAlg::finalize () {
  return StatusCode::SUCCESS;
}
