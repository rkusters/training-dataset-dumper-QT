#include "trackSort.hh"

#include "xAODTracking/TrackParticle.h"

namespace {

  // sort functions
  bool by_d0(const xAOD::TrackParticle* t1,
             const xAOD::TrackParticle* t2) {
    static SG::AuxElement::ConstAccessor<float> d0("btag_ip_d0");
    return std::abs(d0(*t1)) > std::abs(d0(*t2));
  }
  bool by_sd0(const xAOD::TrackParticle* t1,
              const xAOD::TrackParticle* t2) {
    static SG::AuxElement::ConstAccessor<float> d0("btag_ip_d0");
    static SG::AuxElement::ConstAccessor<float> d0s("btag_ip_d0_sigma");
    return std::abs(d0(*t1) / d0s(*t1)) > std::abs(d0(*t2) / d0s(*t2));
  }
  bool by_signed_d0(const xAOD::TrackParticle* t1,
                    const xAOD::TrackParticle* t2) {
    static SG::AuxElement::ConstAccessor<float> d0_signed("IP3D_signed_d0_significance");
    return d0_signed(*t1) > d0_signed(*t2);
  }
  bool by_beamspot_d0(const xAOD::TrackParticle* t1,
                      const xAOD::TrackParticle* t2) {
    return std::abs(t1->d0()) > std::abs(t2->d0());
  }
}

TrackSort trackSort(TrackSortOrder order) {
  switch(order) {
  case TrackSortOrder::ABS_D0_SIGNIFICANCE: return &by_sd0;
  case TrackSortOrder::ABS_D0: return &by_d0;
  case TrackSortOrder::D0_SIGNIFICANCE: return &by_signed_d0;
  case TrackSortOrder::ABS_BEAMSPOT_D0: return &by_beamspot_d0;
  default: throw std::logic_error("undefined sort order");
  }
}
