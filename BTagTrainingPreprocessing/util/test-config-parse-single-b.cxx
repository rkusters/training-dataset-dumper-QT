#include "src/SingleBTagConfig.hh"

#include <filesystem>
#include <iostream>

int main(int narg, char* argv[]) {
  namespace fs = std::filesystem;
  if (narg != 2) {
    std::cout << "usage: " << argv[0] << " <json_file>" << std::endl;
    return 1;
  }

  fs::path config_path(argv[1]);
  if (!fs::exists(config_path)) return 2;
  get_singlebtag_config(config_path.string());

  return 0;
}
