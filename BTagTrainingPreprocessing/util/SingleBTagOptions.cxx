#include "SingleBTagOptions.hh"
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

SingleTagIOOpts get_single_tag_io_opts(int argc, char* argv[]) {
  SingleTagIOOpts file;
  bool split_on_comma;
  namespace po = boost::program_options;
  std::string usage = "usage: " + std::string(argv[0]) + " <files>..."
    + " -c <config-file> [-o <output>] [-h] [opts...]\n";
  po::options_description opt(usage + "\nDump btag training HDF5 from an AOD");
  opt.add_options()
    ("in-file",
     po::value(&file.in)->required()->multitoken(),
     "input file name")
    ("out-file,o",
     po::value(&file.out)->default_value("output.h5"),
     "output file name")
    ("help,h", "Print help messages")
    ("max-events,m", po::value(&file.max_events)->default_value(0, "all"),
     "max events to process")
     ("config-file,c",
      po::value(&file.config_file_name)->required(),
      "name of configuration file to use")
    ("split-files-on-comma,s", po::bool_switch(&split_on_comma),
     "split files on commas (needed for prun)")
    ;

  po::positional_options_description pos_opts;
  pos_opts.add("in-file", -1);

  po::variables_map vm;
  try {
    po::store(po::command_line_parser(argc, argv).options(opt)
              .positional(pos_opts).run(), vm);
    if ( vm.count("help") ) {
      std::cout << opt << std::endl;
      exit(1);
    }
    po::notify(vm);
  } catch (po::error& err) {
    std::cerr << usage << "ERROR: " << err.what() << std::endl;
    exit(1);
  }
  if (split_on_comma) {
    if (file.in.size() > 1) {
      throw std::logic_error(
        "you asked to split inputs on commas but provided"
        " more than one input file, this is probably an error");
    } else if (file.in.size() == 1) {
      std::vector<std::string> strs;
      boost::split(strs, file.in.at(0), boost::is_any_of(", "));
      file.in = strs;
    }
  }
  return file;

}
